from flask import Flask, jsonify, request, Response, url_for
import requests
import socket
import os
import setproctitle

# Setzen des Prozessnamens
setproctitle.setproctitle("webshop")

app = Flask(__name__)

# Sample data
products = [
    {"id": 1, "name": "Land Rover Range Rover", "price": 150000},
    {"id": 2, "name": "Land Rover Discovery", "price": 100000},
    {"id": 3, "name": "Land Rover Defender", "price": 120000},
    {"id": 4, "name": "BMW 3er", "price": 60000},
    {"id": 5, "name": "BMW X5", "price": 100000},
    {"id": 6, "name": "BMW i4", "price": 80000},
    {"id": 7, "name": "Audi A4", "price": 60000},
    {"id": 8, "name": "Audi Q5", "price": 70000},
    {"id": 9, "name": "Audi e-tron", "price": 100000},
    {"id": 10, "name": "Mercedes-Benz C-Klasse", "price": 70000},
    {"id": 11, "name": "Mercedes-Benz GLE", "price": 100000},
    {"id": 12, "name": "Mercedes-Benz EQS", "price": 140000},
    {"id": 13, "name": "Toyota Corolla", "price": 35000},
    {"id": 14, "name": "Toyota RAV4", "price": 45000},
    {"id": 15, "name": "Toyota Highlander", "price": 60000},
    {"id": 16, "name": "Tesla Model 3", "price": 55000},
    {"id": 17, "name": "Tesla Model Y", "price": 70000},
    {"id": 18, "name": "Tesla Model S", "price": 130000},
    {"id": 19, "name": "Ford Mustang", "price": 60000},
    {"id": 20, "name": "Ford Explorer", "price": 70000},
    {"id": 21, "name": "Ford F-150", "price": 70000},
    {"id": 22, "name": "Volkswagen Golf", "price": 45000},
    {"id": 23, "name": "Volkswagen Tiguan", "price": 50000},
    {"id": 24, "name": "Volkswagen ID.4", "price": 60000}
]

customers = [
    {"id": 1, "name": "John Doe", "email": "john.doe@example.com"},
    {"id": 2, "name": "Jane Smith", "email": "jane.smith@example.com"},
    {"id": 3, "name": "Bob Johnson", "email": "bob.johnson@example.com"}
]

orders = [
    {"id": 1, "product_id": 1, "product": "Land Rover Range Rover", "quantity": 1, "customer_id": 1, "customer_name": "John Doe"},
    {"id": 2, "product_id": 4, "product": "BMW 3er", "quantity": 2, "customer_id": 2, "customer_name": "Jane Smith"},
    {"id": 3, "product_id": 8, "product": "Audi Q5", "quantity": 1, "customer_id": 3, "customer_name": "Bob Johnson"},
    {"id": 4, "product_id": 16, "product": "Tesla Model 3", "quantity": 3, "customer_id": 1, "customer_name": "John Doe"},
    {"id": 5, "product_id": 19, "product": "Ford Mustang", "quantity": 1, "customer_id": 2, "customer_name": "Jane Smith"},
    {"id": 6, "product_id": 22, "product": "Volkswagen Golf", "quantity": 2, "customer_id": 3, "customer_name": "Bob Johnson"},
    {"id": 7, "product_id": 10, "product": "Mercedes-Benz C-Klasse", "quantity": 1, "customer_id": 1, "customer_name": "John Doe"}
]

# Routes for Catalog Service
@app.route('/catalog/api', methods=['GET'])
def get_products():
    return jsonify(products)

@app.route('/catalog', methods=['GET'])
def get_products_html():
    hostname = socket.gethostname()
    return f'''
    <html>
    <head>
        <title>Catalog Service on {hostname}</title>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <h1 class="mt-4">Catalog Service on {hostname}</h1>
            <p class="lead">List of products</p>
            <ul class="list-group">
                {''.join([f'<li class="list-group-item">{product["name"]} (${product["price"]})</li>' for product in products])}
            </ul>
        </div>
    </body>
    </html>
    '''

# Routes for Customer Service
@app.route('/customer/api', methods=['GET'])
def get_customers():
    return jsonify(customers)

@app.route('/customer', methods=['GET'])
def get_customers_html():
    hostname = socket.gethostname()
    return f'''
    <html>
    <head>
        <title>Customer Service on {hostname}</title>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <h1 class="mt-4">Customer Service on {hostname}</h1>
            <p class="lead">List of customers</p>
            <ul class="list-group">
                {''.join([f'<li class="list-group-item">{customer["name"]} ({customer["email"]})</li>' for customer in customers])}
            </ul>
        </div>
    </body>
    </html>
    '''

# Routes for Order Service
@app.route('/order/api', methods=['GET'])
def get_orders():
    return jsonify(orders)

@app.route('/order', methods=['GET'])
def get_orders_html():
    hostname = socket.gethostname()
    return f'''
    <html>
    <head>
        <title>Order Service on {hostname}</title>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <h1 class="mt-4">Order Service on {hostname}</h1>
            <p class="lead">List of orders</p>
            <ul class="list-group">
                {''.join([f'<li class="list-group-item">Order #{order["id"]}: {order["product"]} (Quantity: {order["quantity"]}, Customer: {order["customer_name"]}, Customer ID: {order["customer_id"]}, Product ID: {order["product_id"]})</li>' for order in orders])}
            </ul>
        </div>
    </body>
    </html>
    '''

# Reverse Proxy
PREFIX = os.getenv('URL_PREFIX', '')

SERVICES = {
    'customer': f'http://customer{PREFIX}:8080',
    'catalog': f'http://catalog{PREFIX}:8080',
    'order': f'http://order{PREFIX}:8080'
}

@app.route(f'/{PREFIX}')
def index():
    hostname = socket.gethostname()
    customer_url = url_for('get_customers_html')
    catalog_url = url_for('get_products_html')
    order_url = url_for('get_orders_html')

    return f'''
    <html>
    <head>
        <title>Auto Shop on {hostname}</title>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <h1 class="mt-4">Auto Shop on {hostname}</h1>
            <ul class="list-group">
                <li class="list-group-item"><a href="{customer_url}">Customer</a></li>
                <li class="list-group-item"><a href="{catalog_url}">Catalog</a></li>
                <li class="list-group-item"><a href="{order_url}">Order</a></li>
            </ul>
        </div>
    </body>
    </html>
    '''

@app.route(f'/{PREFIX}/<service>/', defaults={'path': ''}, methods=['GET', 'POST', 'PUT', 'DELETE'])
@app.route(f'/{PREFIX}/<service>/<path:path>', methods=['GET', 'POST', 'PUT', 'DELETE'])
def proxy_varied(service, path):
    if service not in SERVICES:
        return Response(f"Service {service} not found", status=404)

    service_url = f"{SERVICES[service]}/{path}"

    if request.method == 'GET':
        response = requests.get(service_url, params=request.args)
    elif request.method == 'POST':
        response = requests.post(service_url, json=request.get_json())
    elif request.method == 'PUT':
        response = requests.put(service_url, json=request.get_json())
    elif request.method == 'DELETE':
        response = requests.delete(service_url)
    else:
        return Response("Method not supported", status=405)

    excluded_headers = ['content-encoding', 'content-length', 'transfer-encoding', 'connection']
    headers = [(name, value) for name, value in response.raw.headers.items() if name.lower() not in excluded_headers]

    return Response(response.content, response.status_code, headers)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
