Autoshop
========

[[_TOC_]]

Einfacher WebShop implementiert als monolithische Applikation.

Dies Applikation kann mehrmals gestartet werden.

Mittels `-e URL_PREFIX=xxx` kann ein Prefix angegeben werden. Das wichtig wenn mehrere Instanzen mittels des gleichen FQDN erreichbar sein sollen, z.B.:
* https://example.com/m1 - wird weitergeleitet an http://localhost:8081
* https://example.com/m2 - wird weitergeleitet an http://localhost:8082

Version 2.0.0
-------------

Version 2.0.0 mit ansprechender Oberfläche.

**Image builden**

    podman build -t registry.gitlab.com/ch-mc-b/autoshop/shop:2.0.0 .

    podman push registry.gitlab.com/ch-mc-b/autoshop/shop:2.0.0

**Applikation starten**

    podman run -d --rm -p 8080:8080 --name shop registry.gitlab.com/ch-mc-b/autoshop/shop:2.0.0     

Testen mittels

* [http://localhost:8080](http://localhost:8080)

**Stoppen und Aufräumen**

    podman stop shop

Version 1.0.0
-------------

Einfache, unsichere Variante. U.a.:
* läuft innerhalb des Containers mit root-Rechten
* keine Begrenzung Memory beim Starten

**Image builden**

    podman build -t registry.gitlab.com/ch-mc-b/autoshop/shop:1.0.0 .

    podman push registry.gitlab.com/ch-mc-b/autoshop/shop:1.0.0

**Applikation starten**

Normaler Start

    podman run -d --rm -p 8080:8080 --name shop registry.gitlab.com/ch-mc-b/autoshop/shop:1.0.0 

Weitere Instanzen, z.B. eine pro Mandant

    podman run -d --rm -p 8081:8080 -e URL_PREFIX=m1 --name shop-m1 registry.gitlab.com/ch-mc-b/autoshop/shop:1.0.0 
    podman run -d --rm -p 8082:8080 -e URL_PREFIX=m2 --name shop-m2 registry.gitlab.com/ch-mc-b/autoshop/shop:1.0.0 

Testen mittels:
* [http://localhost:8080](http://localhost:8080)
* [http://localhost:8081/m1](http://localhost:8081/m1)
* [http://localhost:8082/m2](http://localhost:8082/m2)

Memory Verbrauch beobachten

    podman stats shop

**Stoppen und Aufräumen**

    podman stop shop
    podman stop shop-m1
    podman stop shop-m2

**Hinweis*: `podman container prune` ist nicht nötig, weil `--rm` den Container automatisch aufräumt.

