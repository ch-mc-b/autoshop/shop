# Use an official Python runtime as a parent image
FROM python:3.9-alpine

# Set the working directory in the container
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . /app

# Aktualisiere die Paketliste und installiere benötigte Pakete
RUN apk update && \
    apk add --no-cache gcc musl-dev linux-headers py3-setproctitle && \
    pip install --no-cache-dir -r requirements.txt && \
    apk del gcc musl-dev linux-headers

# Make port 8080 available to the world outside this container
EXPOSE 8080

# Define environment variable
ENV FLASK_APP=webshop.py

# Run the application
CMD ["flask", "run", "--host=0.0.0.0", "--port=8080"]
